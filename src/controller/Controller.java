package controller;


import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.PUBLIC_MEMBER;



import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoSimple;
import model.logic.ManejadorPeliculas;
import model.logic.Taller4;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;
import api.ITaller4;

public class Controller {

	
	private static ITaller4 taller= new Taller4();

	public static void cargarPeliculas() {
		
		taller.cargarArchivoPeliculas("data/movies.csv");
		
	}
public static ListaEncadenada<VOPelicula> darNumeroNPeliculas(int n){
	return taller.darNumeroNPeliculas(n); 
}
public static void arregloParaOrdenar(){
	taller.arregloParaOrdenar(); 
}
public static ArrayList<VOPelicula> darList()
{
	return taller.darList(); 
}
public static ArrayList<VOPelicula> mergeSort(ArrayList<VOPelicula> whole)
{
	return taller.mergeSort(whole); 
}
//	public static ILista<VOPelicula> darListaPeliculas(String busqueda) {
//		return manejador.darListaPeliculas(busqueda);
//	}
//
//	public static ILista<VOPelicula> darPeliculasPorAgno(int agno) {
//		return manejador.darPeliculasAgno(agno);
//	}
//	
//	public static VOAgnoPelicula darPeliculasAgnoSiguiente() {
//		return manejador.darPeliculasAgnoSiguiente();
//	}
//	
//	public static VOAgnoPelicula darPeliculasAgnoAnterior() {
//		return manejador.darPeliculasAgnoAnterior();
//	}
//	public static ILista<VOPelicula> darNumeroNPeliculas(int n){
//		return manejador.darNumeroNPeliculas(n); 
//	}
//	public static ArrayList<VOPelicula> listaOrden()
//	{
//		return manejador.darList(); 
//	}
//	public static void merge(ArrayList<VOPelicula> whole) {
//		manejador.mergeSort(whole); 
//	}
	

}
