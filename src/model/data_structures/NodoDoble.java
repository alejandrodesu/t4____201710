package model.data_structures;

public class NodoDoble<E>
{
	private NodoDoble<E> next;
	
	private NodoDoble<E> prev;
	
	private E item;
	
	public NodoDoble(E item)
	{
		next = null;
		this.item = item;
		prev = null;
	}
	
	public NodoDoble<E> getNext()
	{
		return next;
	}
	
	public NodoDoble<E> getPrev()
	{
		return prev;
	}
	
	public void setNext(NodoDoble<E> next)
	{
		this.next = next;
	}
	
	public void setPrev(NodoDoble<E> prev)
	{
		this.prev = prev;
	}
	
	public E getItem()
	{
		return item;
	}
	
	
}
