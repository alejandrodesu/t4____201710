package model.data_structures;

import java.util.NoSuchElementException;

import javax.management.RuntimeErrorException;
import javax.swing.text.html.HTMLDocument.Iterator;

public class ArrayList<T> implements Iterable<T>
{
	private Object[] array; 
	public static final int DEAFAULT_SIZE = 20; 
	private int size = 20; 
	public ArrayList()
	{
		this(DEAFAULT_SIZE); 
	}
	public ArrayList(int size) {
		array = new Object[size];
	}
	public void add(T element){
		ensureCapacity(); 
		array[size++] = element; 
	}
	public T remove (int index){
		if (index >= size || index < 0){
			throw new RuntimeException("Invalid Index");
		}
		T element = (T) array[index]; 
		--size; 
		compress(); 
		return element; 
	}
	public T set(int index, T e)
	{
		checkBoundExclusive(index);
		T result = (T) array[index];
		array[index] = e;
		return result;
	}
	private void checkBoundExclusive(int index)
	{
		// Implementation note: we do not check for negative ranges here, since
		// use of a negative index will cause an ArrayIndexOutOfBoundsException,
		// a subclass of the required exception, with no effort on our part.
		if (index >= size)
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: "
					+ size);
	}
	public T get(int index){
		if(index > size){throw new RuntimeException("Invalid index");}
		T element = (T) array[index];
		return element;
	}

	public int size(){
		return this.size;
	}

	private void ensureCapacity(){
		if(size < array.length){ return;}
		resize();
	}

	private void resize(){
		Object[] temp = new Object[array.length*2];
		copy(array,temp);
		array = temp;
	}

	private void copy(Object[] src, Object[] dest){
		if(dest.length< src.length){throw new RuntimeException(src+ " cannot be copied into "+dest);}
		for(int i=0;i<src.length; i++){
			dest[i] = src[i];
		}   
	}

	private void compress(){
		int skipCount =0;
		for(int i=0;i < size && i<array.length; i++){
			if(array[i]==null){
				++skipCount;                
			}
			array[i]=array[i+skipCount];
		}
	}
	@Override
	public java.util.Iterator<T> iterator() 
	{
		return new java.util.Iterator<T>() 
				{


			private int index;

			public boolean hasNext() 
			{
				return !(array.length == index);
			}

			public T next() 
			{
				if(hasNext()) {
		            return (T) array[index++];
		        } else {
		            throw new NoSuchElementException("There are no elements size = " + array.length);
		        }

			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub

			}
				};
	}


}
