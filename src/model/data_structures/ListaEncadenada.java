package model.data_structures;

import java.util.Iterator;

import javax.xml.crypto.NodeSetData;

import model.vo.VOPelicula;


public class ListaEncadenada<T> implements ILista<T> {

	private NodoSimple<T> primero2;
	private NodoSimple<T> actual;
	private int listSize;

	public ListaEncadenada()
	{
		primero2 = new NodoSimple<T>();
		actual = primero2;
	}
	public NodoSimple<VOPelicula> darPrimero(){return (NodoSimple<VOPelicula>) primero2;}
	public int size()
	{
		return listSize;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
				{
			private NodoSimple<T> actualT=null;

			public boolean hasNext() 
			{
				if(actualT==null)return primero2.getItem()!=null;
				else return actualT.getNext() != null;
			}

			public T next() 
			{
				if(actualT==null)
				{
					actualT=primero2;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else
				{
					actualT = actualT.getNext();
					return actualT.getItem();
				}

			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub

			}


				};
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero2.getItem() == null)
		{
			primero2.setItem(elem);
			return;
		}
		else
		{
			NodoSimple<T> act = primero2;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setNodoSimple(new NodoSimple<T>());
					act.getNext().setItem(elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero2;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (T) actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		NodoSimple<T> act = primero2;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}
	public NodoSimple<T> getNext()
	{

		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
		}
		return actual; 
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		NodoSimple<T> act = primero2;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}

	@Override
	public void shuffle() 
	{
		NodoSimple<T> actual; 
		NodoSimple<T> random; 
		T temp; 
		int rand; 

		rand = (int)(Math.random() * listSize);
		for (int j = 0 ; j < listSize ; j++)
		{
			actual = primero2; 
			random = primero2; 
			rand = (int)(Math.random() * listSize); 
			for (int k =0; k < rand ; k++)
			{
				random = random.getNext(); 
				temp = actual.getItem(); 
				actual.setItem(random.getItem());
				random.setItem(temp);
			}
		}
	}
	public NodoSimple<T> darUltimo()
	{
		NodoSimple<T> aux = null;
		if (primero2 != null)
		{
			aux = primero2; 
		}

		while (aux.getNext()!=null)
		{
			aux = aux.getNext(); 
		}
		return aux; 
	}


//	public NodoSimple<T> merge_sort(NodoSimple<T> primero2)
//	{
//
//		if (primero2 == null || primero2.getNext() == null){return (NodoSimple<T>) primero2;}
//		NodoSimple<T> middle = getMiddle((NodoSimple<VOPelicula>) primero2);
//		NodoSimple<T> middleNext = middle.getNext(); 
//		NodoSimple<T> sHalf = middleNext; middleNext = null; 
//		return merge(merge_sort(primero2), merge_sort(sHalf));
//	}
//	public NodoSimple<T> merge(NodoSimple<T> a, NodoSimple<T> b)
//	{
//		NodoSimple<VOPelicula> dummyHead, curr; dummyHead = new NodoSimple<>(); curr= dummyHead; 
//		while (a != null && b != null)
//		{
//			NodoSimple<VOPelicula> currNext = curr.getNext();
//			if (((VOPelicula) a.getItem()).compareTo((VOPelicula) b.getItem())<= 0){currNext=(NodoSimple<VOPelicula>) a; a=a.getNext();}
//			else {currNext = (NodoSimple<VOPelicula>) b; b = b.getNext();}
//			curr = curr.getNext(); 
//		}
//		NodoSimple<VOPelicula> currNext = curr.getNext(); 
//		currNext = (NodoSimple<VOPelicula>) ((a == null) ? b : a); 
//		return (NodoSimple<T>) dummyHead.getNext(); 
//	}
//	public NodoSimple<T> getMiddle(NodoSimple<VOPelicula> head)
//	{
//		if(head == null){return (NodoSimple<T>) head;}
//		NodoSimple<VOPelicula> slow, fast; slow=fast=head; 
//		while (fast.getNext() != null && fast.getNext().getNext() != null)
//		{
//			slow = slow.getNext();fast = fast.getNext().getNext(); 
//		}
//		return (NodoSimple<T>) slow; 
//	}
}




