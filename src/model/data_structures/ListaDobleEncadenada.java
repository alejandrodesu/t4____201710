package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> 
{
	private NodoDoble<T> primero;
	
	private NodoDoble<T> ultimo;
	
	private NodoDoble<T> actual;
	
	private int listSize;
	
	public ListaDobleEncadenada()
	{
		primero = null;
		ultimo = null;
		actual = primero;
		listSize = 0;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>(){
			private NodoDoble<T> act = null;
			public boolean hasNext()
			{
				if(act==null)
				{
					return primero!=null;
				}
				else 
					return act.getNext() != null;
			}
			public T next(){
				if(act==null)
				{
					act=primero;
					if(act==null)
					{
						return null;
					}
					else 
						return act.getItem();
				}
				else
				{
					act = act.getNext();
					return act.getItem();
				}
			}
			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodoDoble<T> newNodo = new NodoDoble<T>(elem);
		newNodo.setNext(null);
		if(primero==null)
		{
			primero = newNodo;
			ultimo = newNodo;
		}
		else
		{
			newNodo.setPrev(ultimo);
			ultimo.setNext(newNodo);
			ultimo = newNodo;
		}
		listSize++;
		actual = primero;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		if(actual!=null)
		{	
			for (int i = 0; i < pos; i++) 
			{
				actual = actual.getNext();
			}
			return actual.getItem();
		}
		return null;
	}


	@Override
	public int darNumeroElementos() 
	{
		return size();
	}

	@Override
	public T darElementoPosicionActual() {
		if(actual!=null)
		{
			return actual.getItem();
		}
		else 
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual!=null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		if(actual!=null&&actual.getPrev()!= null)
		{
			actual = actual.getPrev();
			return true;
		}
		return false;
	}

	@Override
	public void shuffle() {
		// TODO Auto-generated method stub
		
	}

}
