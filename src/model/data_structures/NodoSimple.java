package model.data_structures;

public class NodoSimple<T>
{
	private T item;

	private NodoSimple<T> next;

	public NodoSimple()
	{
		item = null;
		next = null;
	}


	public T getItem() 
	{
		return item;
	}

	public void setItem(T item) 
	{
		this.item = item;
	}

	public NodoSimple<T> getNext() 
	{
		return next;
	}


	public void setNodoSimple(NodoSimple<T> newNode) 
	{
		next = newNode;
	}
}
