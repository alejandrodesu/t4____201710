package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import api.ITaller4;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;


public class Taller4 implements ITaller4
{
	private ILista<VOPelicula> misPeliculas;
	private ILista<VOAgnoPelicula> peliculasAgno;
	public ListaEncadenada<VOPelicula> listaOrden;
	public ArrayList<VOPelicula> list = new ArrayList<VOPelicula>(); 
	public ArrayList<VOPelicula> darList()
	{
		return list; 
	}
	public void cargarArchivoPeliculas(String archivoPeliculas) 
	{
		misPeliculas = new ListaEncadenada<VOPelicula>();
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		BufferedReader buffer = null;
		FileReader file = null;
		try
		{
			file = new FileReader("data/movies.csv");
			buffer = new BufferedReader(file);
			String line = buffer.readLine();
			line = buffer.readLine();
			for(int i = 1950; i <= 2016; i++)
			{
				VOAgnoPelicula pActual = new VOAgnoPelicula();
				pActual.setAgno(i);
				pActual.setPeliculas(new ListaDobleEncadenada<VOPelicula>());
				peliculasAgno.agregarElementoFinal(pActual);
			}

			while(line != null)
			{
				String[] datos = new String[4];
				String nombre;
				String generos;
				if(line.indexOf('"') != -1)
				{
					nombre = line.substring(line.indexOf('"') +1 , line.lastIndexOf('"')).trim();
					line = line.substring(0, line.indexOf('"')) + line.substring(line.lastIndexOf('"') + 1, line.length());
					datos = line.split(",");
					generos = datos[1];
				}
				else
				{
					datos = line.split(",");
					nombre = datos[1].trim();
					generos = datos[2];
				}

				String[] arrayGeneros = generos.split("|");
				String titulo = nombre.substring(0, nombre.length()-7);
				String agno = "0";
				if(nombre.charAt(nombre.length()-1) == ')' )
				{
					if(nombre.charAt(nombre.length()-6) == '(')
					{
						agno = nombre.substring(nombre.length()-5, nombre.length()-1);
					}
					else if(nombre.charAt(nombre.length()-6) == '-')
					{
						agno = nombre.substring(nombre.length()-10, nombre.length()-6);
					}
					else if(nombre.charAt(nombre.length()-2)=='-')
					{
						agno = nombre.substring(nombre.length()-6, nombre.length()-2);
					}
				}

				ILista<String> genres = new ListaDobleEncadenada<>();
				for(int i = 0; i<arrayGeneros.length; i++)
				{
					genres.agregarElementoFinal(arrayGeneros[i]);
				}

				VOPelicula act = new VOPelicula();
				act.setTitulo(titulo);
				act.setAgnoPublicacion(Integer.parseInt(agno));
				act.setGenerosAsociados(genres);
				misPeliculas.agregarElementoFinal(act);

				if(act.getAgnoPublicacion()>1950)
				{
					peliculasAgno.darElemento(act.getAgnoPublicacion()-1950).getPeliculas().agregarElementoFinal(act);
				}
				line = buffer.readLine();
			}

			buffer.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public ListaEncadenada<VOPelicula> darNumeroNPeliculas(int n)
	{


		ListaEncadenada<VOPelicula> respuesta = new ListaEncadenada<>(); 
		Iterator<VOPelicula> iter = misPeliculas.iterator(); 
		if (n < misPeliculas.darNumeroElementos())
		{
			misPeliculas.shuffle();
			for (int i = 0 ; i < n ; i++){
				VOPelicula act = iter.next(); 
				respuesta.agregarElementoFinal(act);
			}
			listaOrden=respuesta; 
		}
		else
		{


			misPeliculas.shuffle();
			for (int i = 0 ; i < misPeliculas.darNumeroElementos() ; i++){
				
					VOPelicula act = iter.next(); 
					respuesta.agregarElementoFinal(act);
				

			}
			listaOrden=respuesta; 


		}

		return respuesta; 
	}
	public void arregloParaOrdenar()
	{


		ArrayList<VOPelicula> lista = list; 
		Iterator<VOPelicula> iter = listaOrden.iterator();
		while(iter.hasNext())
		{
			VOPelicula act = iter.next();
			lista.add(act); 


		}


	}
	public ArrayList<VOPelicula> mergeSort(ArrayList<VOPelicula> whole) 
	{
		ArrayList<VOPelicula> left = new ArrayList<VOPelicula>();
		ArrayList<VOPelicula> right = new ArrayList<VOPelicula>();
		int center;

		if (whole.size() == 1) {    
			return whole;
		} else {
			center = whole.size()/2;
			// copy the left half of whole into the left.
			for (int i=0; i<center; i++) {
				left.add(whole.get(i));
			}

			//copy the right half of whole into the new arraylist.
			for (int i=center; i<whole.size(); i++) {
				right.add(whole.get(i));
			}

			// Sort the left and right halves of the arraylist.
			left  = mergeSort(left);
			right = mergeSort(right);

			// Merge the results back together.
			merge(left, right, whole);
		}
		return whole;
	}
	private void merge(ArrayList<VOPelicula> left, ArrayList<VOPelicula> right, ArrayList<VOPelicula> whole) {
		int leftIndex = 0;
		int rightIndex = 0;
		int wholeIndex = 0;

		// As long as neither the left nor the right ArrayList has
		// been used up, keep taking the smaller of left.get(leftIndex)
		// or right.get(rightIndex) and adding it at both.get(bothIndex).
		while (leftIndex < left.size() && rightIndex < right.size()) {
			if ( (left.get(leftIndex).compareTo(right.get(rightIndex))) < 0) {
				whole.set(wholeIndex, left.get(leftIndex));
				leftIndex++;
			} else {
				whole.set(wholeIndex, right.get(rightIndex));
				rightIndex++;
			}
			wholeIndex++;
		}

		ArrayList<VOPelicula> rest;
		int restIndex;
		if (leftIndex >= left.size()) {
			// The left ArrayList has been use up...
			rest = right;
			restIndex = rightIndex;
		} else {
			// The right ArrayList has been used up...
			rest = left;
			restIndex = leftIndex;
		}

		// Copy the rest of whichever ArrayList (left or right) was not used up.
		for (int i=restIndex; i<rest.size(); i++) {
			whole.set(wholeIndex, rest.get(i));
			wholeIndex++;
		}
	}


}
