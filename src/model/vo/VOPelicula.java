package model.vo;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private String titulo;
	private int agnoPublicacion;
	private ILista<String> generosAsociados;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	@Override
	public int compareTo(VOPelicula o) {
		
		final int BEFORE = -1; 
		final int EQUAL = 0; 
		final int AFTER = 1;
		//para titulos
		if (this.getTitulo().compareTo(o.getTitulo())==0)return EQUAL;
		if (this.getTitulo().compareToIgnoreCase(o.getTitulo())<0) return BEFORE; 
		if (this.getTitulo().compareTo(o.getTitulo())>0) return AFTER;
		//para a�os 
		if (this.getAgnoPublicacion() == o.getAgnoPublicacion()) return EQUAL; 
		if (this.getAgnoPublicacion() < o.getAgnoPublicacion()) return BEFORE;
		if(this.getAgnoPublicacion() > o.getAgnoPublicacion()) return AFTER; 
		
		return EQUAL; 
	}
	public boolean equals(VOPelicula o)
	{
		return 
				((this.getTitulo().compareTo(o.getTitulo()) == 0)&&
				(this.getAgnoPublicacion() == o.getAgnoPublicacion()));
	}
	

}
