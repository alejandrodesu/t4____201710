package view;


import java.util.ArrayList;
import java.util.Scanner;





import model.data_structures.ILista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import controller.Controller;

public class VistaManejadorPeliculas {

	public static void main(String[] args) {


		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				Controller.cargarPeliculas();
				break;
			case 2: 
				System.out.println("ingrese numero N de peliculas a cargar");
				int n = sc.nextInt();
				ILista<VOPelicula> list=Controller.darNumeroNPeliculas(n);
				System.out.println("las peliculas solicitadas son: ");
				for (VOPelicula voPelicula : list) {
					System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
				}
				System.out.println(System.currentTimeMillis());
				break;
				
			case 3: 
				Controller.arregloParaOrdenar();
				ArrayList<VOPelicula> lista = Controller.darList();
				for (VOPelicula voPelicula : lista) {
					System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
				}
				break; 
			case 4: 
				Controller.arregloParaOrdenar();
				ArrayList<VOPelicula> whole = Controller.darList();
				
				Controller.mergeSort(whole); 
				for (VOPelicula voPelicula : whole) {
					System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
				}
				System.out.println(System.currentTimeMillis());
				break; 
				

			case 5:	
				fin=true;
				break;
			}


		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Mostrar N numero de Peliculas");
		System.out.println("3. dar Arreglo");
		System.out.println("4. orednar");
		System.out.println("5. salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

}
