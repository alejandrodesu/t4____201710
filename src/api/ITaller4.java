package api;




import java.util.ArrayList;

import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;

public interface ITaller4 
{
	public void cargarArchivoPeliculas(String archivoPeliculas) ;
	public ListaEncadenada<VOPelicula> darNumeroNPeliculas(int n);
	public void arregloParaOrdenar(); 
	public ArrayList<VOPelicula> darList();
	public ArrayList<VOPelicula> mergeSort(ArrayList<VOPelicula> whole); 
}
